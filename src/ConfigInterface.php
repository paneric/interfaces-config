<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Config;

interface ConfigInterface
{
    public function __invoke(): array;
}
